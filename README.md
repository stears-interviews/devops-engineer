# Stears Devops Engineer Coding Homework
Thanks for your application to Stears

This exercise is designed to assess your familiarity with shell (sh) scripting, python, git, and api integrations with a secure mock gitlab pipeline. 
We ask that you attempt as much as you can by providing `production ready code`, so have enough information to assess your system programming experience.

### Deadline
You will be given at most `3 working days` to complete the task. This deadline would be communicated via email.

### Assessment Criteria
Our favourite submissions are -
1. **Clean & simple code** - Minimal, high quality code that achieves the desired outcome.
2. **Professional & reproducible** - Complete solutions with well explained steps and assumptions. Someone else should be able to run your code and reproduce the exact same result.
3. **Rigorous about quality** - Solutions will be reviewed by our senior engineers

### Conduct
By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.


## Coding Homework 

### Guidelines

- Every step of your work must be documented in code and entirely reproducible.
- Use shell scripting
- Include a README.md telling us how to use your code. It should include clear setup instructions (assume no knowledge of the submission or the stack) and document any important assumptions.

### Scenario

The Stears software engineering team has recently introduced pipeline automation into its  CICD work flow. This has greatly improved time to deliver code. This improvement came at a cost. On one side, we delivered better software but noticed the pipelines were not being looked after.

As out new Devops engineer, we need you to provide visibility into how these pipelines are doing. fortunately Gitlab exposes an endpoint proving this information.

```shell
curl --header "Authorization: Bearer xxxxxxxx" -v http://34.117.217.236/devops-interview/v1/pipeline | jq .
```

##### Task 1: Shell script wrangling

Your first task is to aggregate pipeline data, grouping them by job status and status reason in a `CSV` using the example format below. 

```shell
Status,     Reason,                          Occurrences
success,    n/a,                             12
failed,     cannot_reach_downstream_entity,  2   
failed,     invalid_yaml,                    1
processing, n/a                              3   
```
* please note what the API supports multiple `Reason's` for a failed pipeline. It's your job to make sure you script supports all reasons. 
* the failure reason `think_it_passed` should be ignored from the CSV

Once you have created the table described above, check the table for data quality issues, and fix them as needed.

Export the table as a CSV file called `pipeline_job_summary.csv`

##### Task 2: Python visualisation creation

Use a python `library` of your choice to generate an infographic e.g. a `piechart`, showing a graphic distribution of the results in `pipeline_job_summary.csv`. 

1. Save the infographic in `pipeline_job_summary.jpg`
2. ( Bonus ) Provide a single script that triggers the data fetch from the pipeline api, result aggregation and python infographic creation `run.sh`
3. Provide any instructions around runtime dependencies needed to run the code  (platforms needing install, configuration etc..) in your `README.md`, where needed

![sample pie chart](pics/pic1.jpg)
### Submission

* Create a private GitLab repository and add the following people as maintainers:
  - @adeola.ojo1
  - @oladele_abeeb
  - @ibitolamayowa 
  
- Push all your work to your GitLab repository. It should contain the four files you have created:
  - README.md
  - <folder_with_shell_code>
  - <folder_with_python_code>
  - pipeline_job_summary.csv
  - pipeline_job_summary.jpg
  - run.sh (bonus)

### Have clarification needs?

Do not hesitate to reach out to us, where clarification is needed. We are more than happy to provide any needed context